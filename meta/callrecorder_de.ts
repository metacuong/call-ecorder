<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de">
<context>
    <name></name>
    <message id="tr_callrecorder">
        <location filename="../confirm/src/saveconfirmation.cpp" line="37"/>
        <location filename="../src/aboutpage.h" line="32"/>
        <source></source>
        <oldsource>Call Recorder</oldsource>
        <translation>Call Recorder</translation>
    </message>
    <message id="tr_save_confirmation">
        <location filename="../confirm/src/saveconfirmation.cpp" line="38"/>
        <source></source>
        <oldsource>Do you want to save the record file : %1 ?</oldsource>
        <translation>Wollen Sie die Aufnahme speichern : %1 ?</translation>
    </message>
    <message id="tr_about_title">
        <location filename="../src/aboutpage.cc" line="23"/>
        <source></source>
        <oldsource>About</oldsource>
        <translation>Über</translation>
    </message>
    <message id="tr_thanks_about">
        <location filename="../src/aboutpage.cc" line="32"/>
        <source></source>
        <translation>Danke</translation>
    </message>
    <message id="tr_support_and_feedback">
        <location filename="../src/aboutpage.cc" line="44"/>
        <source></source>
        <oldsource>Support and Feedback</oldsource>
        <translation  >Hilfe und Feedback</translation>
    </message>
    <message id="tr_contributors_about">
        <source></source>
        <translation type="obsolete">Contributors</translation>
    </message>
    <message id="tr_controller_title">
        <location filename="../src/controllerpage.cc" line="24"/>
        <source></source>
        <oldsource>Controller</oldsource>
        <translation  >Einstellungen</translation>
    </message>
    <message id="tr_services_start">
        <location filename="../src/controllerpage.cc" line="27"/>
        <source></source>
        <oldsource>Start</oldsource>
        <translation>Start</translation>
    </message>
    <message id="tr_services_stop">
        <location filename="../src/controllerpage.cc" line="30"/>
        <source></source>
        <oldsource>Stop</oldsource>
        <translation>Stop</translation>
    </message>
    <message id="tr_services_label">
        <location filename="../src/controllerpage.cc" line="33"/>
        <source></source>
        <oldsource>Manually start services</oldsource>
        <translation  >Service manuell starten</translation>
    </message>
    <message id="tr_autostart_label">
        <location filename="../src/controllerpage.cc" line="37"/>
        <source></source>
        <oldsource>Autostart services</oldsource>
        <translation  >Service automatisch starten</translation>
    </message>
    <message id="tr_autostart_enable">
        <location filename="../src/controllerpage.cc" line="42"/>
        <source></source>
        <oldsource>Enable</oldsource>
        <translation  >Aktivieren</translation>
    </message>
    <message id="tr_autostart_disable">
        <location filename="../src/controllerpage.cc" line="45"/>
        <source></source>
        <oldsource>Disable</oldsource>
        <translation  >Deaktivieren</translation>
    </message>
    <message id="tr_audio_format">
        <location filename="../src/controllerpage.cc" line="73"/>
        <source></source>
        <oldsource>Audio output format</oldsource>
        <translation  >Audioausgabeformat</translation>
    </message>
    <message id="tr_save_option">
        <location filename="../src/controllerpage.cc" line="94"/>
        <source></source>
        <oldsource>File save option</oldsource>
        <translation  >Speicheroptionen</translation>
    </message>
    <message id="tr_just_save">
        <location filename="../src/controllerpage.cc" line="96"/>
        <source></source>
        <oldsource>Just Save</oldsource>
        <translation  >Speichern</translation>
    </message>
    <message id="tr_notify_save">
        <location filename="../src/controllerpage.cc" line="97"/>
        <source></source>
        <oldsource>Notify after save</oldsource>
        <translation  >Nachricht nach dem Speichern</translation>
    </message>
    <message id="tr_ask_save">
        <location filename="../src/controllerpage.cc" line="98"/>
        <source></source>
        <oldsource>Ask before save</oldsource>
        <translation  >Vor dem Speichern nachfragen</translation>
    </message>
    <message id="tr_auto_fetching_contact_name">
        <location filename="../src/controllerpage.cc" line="115"/>
        <source></source>
        <translation  >Kontaktnamen automatisch abrufen</translation>
    </message>
    <message id="tr_change_the_store_folder">
        <location filename="../src/controllerpage.cc" line="131"/>
        <location filename="../src/controllerpage.cc" line="295"/>
        <source></source>
        <translation  >Speicherort ändern</translation>
    </message>
    <message id="tr_phone_banner">
        <location filename="../src/controllerpage.cc" line="229"/>
        <source></source>
        <oldsource>Phone service starting</oldsource>
        <translation  >Telefonservice starten</translation>
    </message>
    <message id="tr_recorder_banner">
        <location filename="../src/controllerpage.cc" line="234"/>
        <source></source>
        <oldsource>Recorder service starting</oldsource>
        <translation  >Aufnahmeservice starten</translation>
    </message>
    <message id="tr_auto_fetching_contact_warning">
        <location filename="../src/controllerpage.cc" line="259"/>
        <source></source>
        <translation  >Warnung: Wenn Sie dieses Feature aktivieren kann dies das Programm verlangsamen (abhängig von Ihrer Kontaktanzahl).</translation>
    </message>
    <message id="tr_records_title">
        <location filename="../src/mainpage.cc" line="23"/>
        <source></source>
        <oldsource>Record Files</oldsource>
        <translation  >Aufnahmedatein</translation>
    </message>
    <message id="tr_open">
        <location filename="../src/mainpage.cc" line="165"/>
        <source></source>
        <oldsource>Open...</oldsource>
        <translation  >Öffnen</translation>
    </message>
    <message id="tr_in_month">
        <location filename="../src/mainpage.cc" line="251"/>
        <source></source>
        <translation  >in %1 Monat</translation>
    </message>
    <message id="tr_in_months">
        <location filename="../src/mainpage.cc" line="253"/>
        <source></source>
        <translation  >in %1 Monaten</translation>
    </message>
    <message id="tr_show_all">
        <location filename="../src/mainpage.cc" line="256"/>
        <source></source>
        <translation  >Zeige Alle</translation>
    </message>
    <message id="tr_limit_number_of_files">
        <location filename="../src/mainpage.cc" line="258"/>
        <source></source>
        <oldsource>Limit number of files</oldsource>
        <translation  >Limitiere die Anzahl der Datein</translation>
    </message>
    <message id="tr_delete_files">
        <location filename="../src/mainpage.cc" line="261"/>
        <source></source>
        <translation  >Lösche Datei</translation>
    </message>
    <message id="tr_delete">
        <location filename="../src/deletefiles.cc" line="31"/>
        <location filename="../src/deletefiles.cc" line="105"/>
        <location filename="../src/mainpage.cc" line="170"/>
        <location filename="../src/mainpage.cc" line="189"/>
        <location filename="../src/playback.cc" line="177"/>
        <source></source>
        <oldsource>Delete</oldsource>
        <translation  >Löschen</translation>
    </message>
    <message id="tr_cancel">
        <location filename="../src/deletefiles.cc" line="34"/>
        <location filename="../src/share.cc" line="32"/>
        <source></source>
        <oldsource>Cancel</oldsource>
        <translation  >Abbrechen</translation>
    </message>
    <message id="tr_are_you_sure">
        <location filename="../src/controllerpage.cc" line="260"/>
        <location filename="../src/controllerpage.cc" line="293"/>
        <location filename="../src/deletefiles.cc" line="103"/>
        <location filename="../src/mainpage.cc" line="187"/>
        <source></source>
        <translation  >Sind Sie sicher?</translation>
    </message>
    <message id="tr_unmark_all">
        <location filename="../src/deletefiles.cc" line="127"/>
        <source></source>
        <oldsource>Unmark all</oldsource>
        <translation  >Auswahl aufheben</translation>
    </message>
    <message id="tr_mark_all">
        <location filename="../src/deletefiles.cc" line="127"/>
        <source></source>
        <oldsource>Mark all</oldsource>
        <translation  >Alle markieren</translation>
    </message>
    <message id="tr_share">
        <location filename="../src/mainpage.cc" line="175"/>
        <location filename="../src/playback.cc" line="178"/>
        <source></source>
        <oldsource>Share...</oldsource>
        <translation  >Teilen</translation>
    </message>
    <message id="tr_file_deleted">
        <location filename="../src/model/recordfilemodel.cpp" line="98"/>
        <source></source>
        <oldsource>The record file(s) has been deleted.</oldsource>
        <translation  >Die Aufnahmedatei(n) wurde(n) gelöscht</translation>
    </message>
    <message id="tr_cant_delete">
        <location filename="../src/model/recordfilemodel.cpp" line="102"/>
        <source></source>
        <oldsource>Can not delete the record file.</oldsource>
        <translation  >Aufnahmedatei kann nicht gelöscht werden</translation>
    </message>
    <message id="tr_today">
        <location filename="../src/model/recordfilemodel.cpp" line="171"/>
        <source></source>
        <oldsource>Today</oldsource>
        <translation  >Heute</translation>
    </message>
    <message id="tr_yesterday">
        <location filename="../src/model/recordfilemodel.cpp" line="173"/>
        <source></source>
        <oldsource>Yesterday</oldsource>
        <translation  >Gestern</translation>
    </message>
    <message id="tr_days_ago">
        <location filename="../src/model/recordfilemodel.cpp" line="175"/>
        <source></source>
        <translation  >vor %1 Tagen</translation>
    </message>
    <message id="tr_lastweek">
        <location filename="../src/model/recordfilemodel.cpp" line="177"/>
        <source></source>
        <translation  >Letzte Woche</translation>
    </message>
    <message id="tr_weeks_ago">
        <location filename="../src/model/recordfilemodel.cpp" line="179"/>
        <source></source>
        <translation  >vor %1 Wochen</translation>
    </message>
    <message id="tr_lastmonth">
        <location filename="../src/model/recordfilemodel.cpp" line="181"/>
        <source></source>
        <translation  >Letztes Monat</translation>
    </message>
    <message id="tr_older">
        <location filename="../src/model/recordfilemodel.cpp" line="183"/>
        <source></source>
        <oldsource>Older</oldsource>
        <translation  >Älter</translation>
    </message>
    <message id="tr_scan">
        <location filename="../src/share.cc" line="33"/>
        <source></source>
        <translation>Scan</translation>
    </message>
    <message id="tr_failed_to_send_the_file">
        <location filename="../src/share.cc" line="98"/>
        <source></source>
        <translation  >Fehler beim Senden der Datei</translation>
    </message>
    <message id="tr_transfer_init">
        <location filename="../src/share.cc" line="103"/>
        <source></source>
        <translation  >Transfer initialisieren</translation>
    </message>
    <message id="tr_completed">
        <location filename="../src/share.cc" line="118"/>
        <source></source>
        <oldsource>Completed</oldsource>
        <translation  >Fertig</translation>
    </message>
    <message id="tr_transferring">
        <location filename="../src/share.cc" line="118"/>
        <source></source>
        <translation  >Transferieren</translation>
    </message>
    <message id="tr_transfer_completed">
        <location filename="../src/share.cc" line="144"/>
        <source></source>
        <translation  >Transfer fertig</translation>
    </message>
    <message id="tr_bluetooth_UnknownError">
        <location filename="../src/share.cc" line="147"/>
        <source></source>
        <translation  >Gerät oder Benutzer hat den Transfer abgebrochen</translation>
    </message>
    <message id="tr_bluetooth_FileNotFoundError">
        <location filename="../src/share.cc" line="148"/>
        <source></source>
        <translation  >Ausgewählte Datei kann nicht geöffnet werden</translation>
    </message>
    <message id="tr_bluetooth_HostNotFoundError">
        <location filename="../src/share.cc" line="149"/>
        <source></source>
        <translation  >Verbindung zum Zielhost nicht möglich</translation>
    </message>
    <message id="tr_bluetooth_UserCanceledTransferError">
        <location filename="../src/share.cc" line="150"/>
        <source></source>
        <translation  >Benutzer hat den Transfer abgebrochen</translation>
    </message>
    <message id="tr_authorized_paired">
        <location filename="../src/share.cc" line="200"/>
        <source></source>
        <translation  >Autorisation gepaart</translation>
    </message>
    <message id="tr_paired">
        <location filename="../src/share.cc" line="200"/>
        <source></source>
        <translation  >Gepaart</translation>
    </message>
    <message id="tr_no_paired">
        <location filename="../src/share.cc" line="200"/>
        <source></source>
        <translation  >Nicht gepaart</translation>
    </message>
    <message id="tr_email">
        <location filename="../src/share.cc" line="227"/>
        <source></source>
        <translation>Email</translation>
    </message>
    <message id="tr_bluetooth">
        <location filename="../src/share.cc" line="227"/>
        <source></source>
        <translation>Bluetooth</translation>
    </message>
    <message id="tr_share_via">
        <location filename="../src/share.cc" line="229"/>
        <source></source>
        <translation  >Teile via</translation>
    </message>
    <message id="tr_done">
        <location filename="../src/playback.cc" line="44"/>
        <source></source>
        <translation  >Fertig</translation>
    </message>
    <message id="tr_phone_number_label">
        <location filename="../src/playback.cc" line="114"/>
        <source></source>
        <translation  >Telefonnummer</translation>
    </message>
    <message id="tr_im_id">
        <location filename="../src/playback.cc" line="114"/>
        <source></source>
        <oldsource>(or IM)</oldsource>
        <translation  >(oder IM)</translation>
    </message>
    <message id="tr_call_type_label">
        <location filename="../src/playback.cc" line="115"/>
        <source></source>
        <translation  >Ruftypus</translation>
    </message>
    <message id="tr_file_format_label">
        <location filename="../src/playback.cc" line="116"/>
        <source></source>
        <translation  >Dateiformat</translation>
    </message>
    <message id="tr_file_size_label">
        <location filename="../src/playback.cc" line="117"/>
        <source></source>
        <translation  >Dateigröße</translation>
    </message>
    <message id="tr_recorded_date_label">
        <location filename="../src/playback.cc" line="118"/>
        <source></source>
        <translation  >Aufnahmedatum</translation>
    </message>
    <message id="tr_duration_label">
        <location filename="../src/playback.cc" line="119"/>
        <source></source>
        <translation  >Dauer</translation>
    </message>
    <message id="tr_play">
        <location filename="../src/playback.cc" line="176"/>
        <location filename="../src/playback.cc" line="267"/>
        <location filename="../src/playback.cc" line="310"/>
        <source></source>
        <translation  >Abspielen</translation>
    </message>
    <message id="tr_note_label">
        <location filename="../src/playback.cc" line="234"/>
        <source></source>
        <translation  >Notiz</translation>
    </message>
    <message id="tr_write_a_note">
        <location filename="../src/playback.cc" line="240"/>
        <source></source>
        <oldsource>Write a note</oldsource>
        <translation  >Notiz schreiben</translation>
    </message>
    <message id="tr_stop">
        <location filename="../src/playback.cc" line="267"/>
        <source></source>
        <translation>Stop</translation>
    </message>
    <message id="tr_incoming_label">
        <location filename="../src/playback.cc" line="379"/>
        <source></source>
        <translation  >Eingehend</translation>
    </message>
    <message id="tr_outgoing_label">
        <location filename="../src/playback.cc" line="379"/>
        <source></source>
        <translation  >Ausgehend</translation>
    </message>
    <message id="tr_version_about">
        <location filename="../src/aboutpage.h" line="34"/>
        <source></source>
        <translation>Version</translation>
    </message>
    <message id="tr_license_about">
        <location filename="../src/aboutpage.h" line="35"/>
        <source></source>
        <translation  >Lizenz</translation>
    </message>
    <message id="tr_author_about">
        <location filename="../src/aboutpage.h" line="36"/>
        <source></source>
        <translation  >Autor</translation>
    </message>
    <message id="tr_search">
        <location filename="../src/searchbox.cc" line="33"/>
        <source></source>
        <oldsource>Search</oldsource>
        <translation  >Suche</translation>
    </message>
</context>
</TS>
